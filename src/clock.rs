use core::{num::Wrapping, time::Duration};

use firmware_common::async_await::{MonotonicComparable, RealTime};
use rtic::{Fraction, Monotonic};
use stm32g0xx_hal::{prelude::*, rcc::Rcc, stm32::TIM14, timer::stopwatch::Stopwatch};

static mut CLOCK: Option<Stopwatch<TIM14>> = None;

pub unsafe fn init(timer: TIM14, rcc: &mut Rcc) {
    let mut sw = timer.stopwatch(rcc);
    sw.set_prescaler(32_000);
    CLOCK = Some(sw);
}

pub struct TimerClock;

// Assumes a 500 us clock period.
impl Monotonic for TimerClock {
    type Instant = Wrapping<i16>;

    fn ratio() -> Fraction {
        Fraction {
            numerator: 1,
            denominator: 64_000,
        }
    }

    fn now() -> Self::Instant {
        Wrapping(unsafe { CLOCK.as_ref().unwrap().now() }.0 as i16)
    }

    unsafe fn reset() {
        CLOCK.as_mut().unwrap().reset();
    }

    fn zero() -> Self::Instant {
        Wrapping(0)
    }
}

impl MonotonicComparable for TimerClock {
    fn zero_diff() -> Wrapping<i16> {
        Wrapping(0)
    }
}

impl RealTime for TimerClock {
    fn offset(base: Self::Instant, offset: Duration) -> Self::Instant {
        base + Wrapping((offset.as_micros() / 500) as i16)
    }
}
