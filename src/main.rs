// This Source Code Form is subject to the terms of the Mozilla Public License,
// v. 2.0. If a copy of the MPL was not distributed with this file, You can
// obtain one at http://mozilla.org/MPL/2.0/.

#![no_main]
#![no_std]

use core::{num::Wrapping, time::Duration};
use firmware_common::async_await::{RealTime, YieldingExecutor};
use rtic::{pend, Monotonic};
use stm32g0xx_hal::{
    prelude::*,
    rcc::{Config, PllConfig, SysClockSrc},
    stm32::{Interrupt, TIM16},
    time::MicroSecond,
    timer::Timer,
};

#[cfg(debug_assertions)]
use cortex_m_semihosting::*;

#[cfg(debug_assertions)]
use panic_semihosting as _;

#[cfg(not(debug_assertions))]
use panic_halt as _;

mod clock;

#[rtic::app(device = stm32g0xx_hal::stm32, peripherals = true, monotonic = crate::clock::TimerClock)]
const APP: () = {
    struct Resources {
        exec: YieldingExecutor<clock::TimerClock>,
        timer: Timer<TIM16>,
    }

    #[init]
    fn init(cx: init::Context) -> init::LateResources {
        // Cortex-M peripherals
        let mut _core = cx.core;

        // Device specific peripherals
        let device = cx.device;

        // Setup clocks
        let rcc = device.RCC.constrain();
        let mut clocks =
            rcc.freeze(Config::new(SysClockSrc::PLL).pll_cfg(PllConfig::with_hsi(1, 16, 4)));

        unsafe {
            clock::init(device.TIM14, &mut clocks);
        }

        #[cfg(debug_assertions)]
        hprintln!("init").unwrap();

        let mut timer: Timer<TIM16> = device.TIM16.timer(&mut clocks);
        timer.listen();

        init::LateResources {
            exec: YieldingExecutor::new(Interrupt::TIM16),
            timer,
        }
    }

    #[task(binds = TIM16, resources = [&exec, timer], priority = 7)]
    fn exec_step(cx: exec_step::Context) {
        cx.resources.timer.clear_irq();

        unsafe {
            cx.resources
                .exec
                .step(cx.resources.timer, |Wrapping(diff)| {
                    MicroSecond((diff as u32) * 500)
                });
        }
    }

    #[idle(resources = [&exec])]
    fn idle(cx: idle::Context) -> ! {
        let loop_future = async {
            pend(Interrupt::SPI2);

            let mut next = clock::TimerClock::now();
            loop {
                #[cfg(debug_assertions)]
                hprintln!("loop").unwrap();

                next = clock::TimerClock::offset(next, Duration::from_secs(1));
                cx.resources.exec.delay_until(next).await;
            }
        };
        unsafe { cx.resources.exec.init_heapless(loop_future) }
    }

    extern "C" {
        fn SPI1();
    }
};
